<?php

require  __DIR__ . '/../vendor/autoload.php';

$config = new \MMC\Statistics\BaiDu\Config(array(
    'username'      => '####',
    'password'      => '####',
    'token'         => '####',
    'uuid'          => '####',
    'account_type'  => 1,  //1:站长账号  3:联盟账号
));


$parameter = array(
    'reportid'      => 1,
    'siteid'        => '6214893',
    'metrics'       => array('pageviews', 'visitors', 'ips', 'entrances', 'outwards', 'exits', 'stayTime', 'exitRate'),
    'dimensions'    => array('pageid'),
    'start_time'    => '20160601000000',
    'end_time'      => '20160601235959',
    'filters'       => array(),
    'start_index'   => 0,
    'max_results'   => 3000,
    'sort'          => array('pageviews desc'),
);

$statistics = new \MMC\Statistics\BaiDu\BaiDu($config);

try {
    $result = $statistics->getStatisticsData($parameter);
    echo $result;
} catch (Exception $e){
    echo $e->getMessage();
}


