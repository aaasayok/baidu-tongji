<?php

namespace MMC\Statistics;

use MMC\Statistics\BaiDu\Config;

interface StatisticsInterface
{

    public function __construct(Config $config);


    public function getStatisticsData(array $conditions);

}