<?php
/**
 * Created by PhpStorm.
 * User: GIGABYTE
 * Date: 2016/6/21
 * Time: 19:20
 */

namespace MMC\Statistics\BaiDu;


class Config
{
    private $config = array(
        'username'      => '',
        'password'      => '',
        'token'         => '',
        'uuid'          => '',
        'account_type'  => 1,
    );

    public function __construct(array $config)
    {
        $this->config = array_merge($this->config, $config);
    }

    public function getUserName()
    {
        return $this->config['username'];
    }

    public function setUserName($username)
    {
        $this->config['username'] = $username;

        return $this;
    }

    public function getPassword()
    {
        return $this->config['password'];
    }

    public function setPassword($password)
    {
        $this->config['password'] = $password;

        return $this;
    }

    public function getToken()
    {
        return $this->config['token'];
    }

    public function setToken($token)
    {
        $this->config['token'] = $token;

        return $this;
    }

    public function getUUID()
    {
        if (!$this->config['uuid']) {
            $this->config['uuid'] = substr(uniqid(md5($this->getUserName())), 0, 32);
        }

        return $this->config['uuid'];
    }

    public function setUUID($uuid)
    {
        $this->config['uuid'] = $uuid;

        return $this;
    }

    public function getAccountType()
    {
        return $this->config['account_type'];
    }

    public function setAccountType($type)
    {
        $this->config['account_type'] = $type;

        return $this;
    }

    public function getExtra($name)
    {
        return isset($this->config[$name]) ? $this->config[$name] : false;
    }

    public function setExtra($name, $value)
    {
        $this->config[$name] = $value;

        return $this;
    }


}