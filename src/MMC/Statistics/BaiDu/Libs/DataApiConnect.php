<?php
/**
 * Created by PhpStorm.
 * User: GIGABYTE
 * Date: 2016/6/21
 * Time: 18:31
 */

namespace MMC\Statistics\BaiDu\Libs;


use MMC\Statistics\BaiDu\Config;

class DataApiConnect extends AbstractClient
{
    public function init(Config $config, $url)
    {
        $this->url = $url;

        $this->headers = array('UUID: '. $config->getUUID(), 'USERID: '.$config->getExtra('ucid'), 'Content-Type:  data/json;charset=UTF-8');
    }


    public function genPostData($data)
    {
        $this->postData = json_encode($data);
    }

    public function parseResponse($response)
    {
        $data = json_decode($response,TRUE);

        if  (isset($data['header']) && isset($data['body'])) {
            return array(
                'ret_head' => $data['header'],
                'ret_body' => $data['body'],
            );
        }

        throw new \Exception("[error] SERVICE ERROR: " . $response . "\r\n");
    }

}