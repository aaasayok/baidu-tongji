<?php
/**
 * Created by PhpStorm.
 * User: GIGABYTE
 * Date: 2016/6/21
 * Time: 18:05
 */

namespace MMC\Statistics\BaiDu\Libs;


abstract class AbstractClient
{
    protected $url = '';

    protected $headers = array();

    protected $userAgent = 'MMC';

    protected $postData;

    abstract public function genPostData($data);

    abstract public function parseResponse($response);

    public function post($data)
    {
        $this->genPostData($data);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);  //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, $this->userAgent);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->postData);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($curl);

        if (curl_errno($curl)) {
            throw new \Exception("CULR ERROR: " . curl_errno($curl));
        }

        curl_close($curl);

        return $this->parseResponse($response);
    }


}