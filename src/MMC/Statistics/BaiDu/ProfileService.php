<?php
/**
 * Created by PhpStorm.
 * User: GIGABYTE
 * Date: 2016/6/22
 * Time: 16:00
 */

namespace MMC\Statistics\BaiDu;

class ProfileService extends AbstractService
{
    const METHOD_GET_SITES = 'getsites';
    const METHOD_GET_TRANS = 'get_trans_info';


    public function query($userId, $st, $parameterJson = '', $method = self::METHOD_GET_SITES)
    {
        $this->config->setExtra('ucid', $userId);

        $this->httpConnect->init($this->config, self::API_URL);

        $apiConnectionData = array(
            'header' => array(
                'username'      => $this->config->getUserName(),
                'password'      => $st,
                'token'         => $this->config->getToken(),
                'account_type'  => $this->config->getAccountType(),
            ),
            'body' => array(
                'serviceName'   => 'profile',
                'methodName'    => $method,
                'parameterJSON' => $parameterJson,
            )
        );

        return $this->httpConnect->post($apiConnectionData);
    }

}