<?php
/**
 * Created by PhpStorm.
 * User: GIGABYTE
 * Date: 2016/6/22
 * Time: 16:56
 */

namespace MMC\Statistics\BaiDu;


use MMC\Statistics\BaiDu\Libs\AbstractClient;

abstract class AbstractService
{
    const LOGIN_URL = 'https://api.baidu.com/sem/common/HolmesLoginService';
    const API_URL   = 'https://api.baidu.com/json/tongji/v1/ProductService/api';

    protected $config;

    protected $httpConnect;

    public function __construct(Config $config, AbstractClient $abstractClient)
    {
        $this->config = $config;

        $this->httpConnect = $abstractClient;
    }

}